.. DM1 Android documentation master file, created by
   sphinx-quickstart on Fri Apr  5 12:49:44 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Dokumentácia DM1 aplikácie pre Android
======================================

Vítám ťa v dokumentácii aplikácie s jednoduchým názvom **DM1**. Pokiaľ túto skratku nepoznáš, pravdepodobne si zablúdil.

DM1 je mobilná aplikácia zameranám na rodičov detí s diagnózou **diabetes mellitus 1. typu** (autoimúnna forma cukrovky, ktorá vyžaduje inzulínovú substitúciu). 

.. _doc_screenshots:

.. image:: ./_static/dashboard.png
  :scale: 15 %
  :alt: Hlavná obrazovka
.. image:: ./_static/notifikacia1.png
  :scale: 15 %
  :alt: Notifikácia
.. image:: ./_static/carelink.png
  :scale: 15 %
  :alt: obrazovka pohľadu na CareLink
.. image:: ./_static/nightscout.png
  :scale: 15 %
  :alt: obrazovka pohľadu na NightScout

.. note:: DM1 aplikácia je vytváraná vo voľnom čase a je vo veľmi skorej fáze vývoja.
          Pri jej používaní maj na zreteli, že môže obsahovať chyby a jej funkcionalita sa bude
          meniť hlavne podľa potrieb autora. 
          
          Za každý tebou :ref:`zaslaný podnet<doc_contact>` na opravu a vylepšenie budem vďačný a
          prispeje k zvýšeniu kvality aplikácie.
..  TODO ako kontaktovat autora

.. caution:: Aplikácia je v stave v akom je. Jej používanie je na vlastnú zodpovednosť. 
          Informácie a opdorúčania v nej majú len informatívny charakter, nenahrádzajú 
          vlastný úsudok užívateľa a už vôbec nie lekárske odporúčania.

..   Submit an issue or pull request on the `GitLab repository
..   <https://github.com/godotengine/godot-docs/issues>`_,
..   help us `translate the documentation
..   <https://hosted.weblate.org/engage/godot-engine/>`_ into your
..   language, or talk to us on either the ``#documentation``
..   channel on `Discord <https://discord.gg/zH7NUgz>`_, or the
..   ``#godotengine-doc`` channel on `irc.freenode.net
..   <http://webchat.freenode.net/?channels=#godotengine-doc>`_!

.. toctree::
   :maxdepth: 2
   :caption: Všeobecne
   :name: sec-general

   about/introduction
   about/features
   about/faq
   
..    about/index

.. toctree::
   :maxdepth: 2
   :caption: Používanie
   :name: sec-usage

   usage/installation
   usage/legal
   usage/quickstart

.. toctree::
   :maxdepth: 2
   :caption: Obrazovky
   :name: sec-screens

   screens/dashboard
   screens/settings
   screens/carelink

.. toctree::
   :maxdepth: 2
   :caption: Iné
   :name: sec-next

   links
..    next/index
