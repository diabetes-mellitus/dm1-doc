.. _doc_quickstart:

Ako začať
=========

Aplikáciu je potrebné :ref:`nainštalovať<doc_install>`. 
Po prvom spustení program nevie, kto je užívateľ ani, kto bude sledovaným diabetikom.

DM1 identifikuje osoby podľa emailovej adresy, preto sa zobrazí dialógové okno Androidu, 
v ktorom vyberieš svoj email. Nevyźaduje sa vytvorenie konta ani zadávanie hesla. Stačí, 
že Android ťa pod týmto emailom pozná.

Notifikačný server si priradí tvoj email k tejto inštalácii. DM1 môžeš používať s tvojím emailom aj na viacerých zariadeniach súčasne.

Okamžite môžeš začať používať :ref:`kalorickú kalkulačku<doc_calc_su>`.

Ďalšie funkcie vyžadujú zadanie informácií o diabetikovi.
Nato klikni v pravo hore ikonku menu a vyber položku **Nastavenia**.

.. image:: ../_static/menu_main.png
  :scale: 15 %
  :alt: Hlavné menu

Dalej vyber **Účet diabetika**. Pomenuj účet, menom alebo nejakou skratkou.

Pokiaľ používaš Medtronic CareLink, tak zadaj účet a heslo.

.. image:: ../_static/pref_patient.png
  :scale: 15 %
  :alt: Nastavenia účtu diabetika

Prepojenie na CareLink nie je okamžité. Môže to trvať aj 24 hodín, kým sa ti začnú zobrazovať údaje z CareLinku
v DM1. Pokiaľ to trvá dlhšie, tak :ref:`kontaktuj autora<doc_contact>`.