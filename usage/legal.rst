.. _doc_legal:

Právne záležitosti
==================

Nemám veľmi čas sa venovať tejto sekcii. 
Hlavná zásada znie: nepoužívajte DM1 pokiaľ máte akýkoľvek pocit nedôvery voči aplikácii a autorovi.

Podmienky použitia
------------------

Udeľujem Vám právo použivať DM1 pri dodržaní nasledovných podmienok:

- beriete na vedomie, že nenesiem žiadnu zodpovednosť za prípadné následky spôsobené používaním DM1
- nič si voči mne a aplikácii nenárokujete
- nahlásite mi chybu, ktoré v aplikácii objavíte

Bezpečnosť a zálohovanie
------------------------

Za nič neručím. Samotné dáta neobsahujú inú identifikáciu akú zdielate svojim emailovým účtom 
a nastavením CareLink účtu.

.. Cookie Policy

Zásady ochrany osobných údajov
------------------------------

- poznám emailový účet, ktorým sa identifikujete v aplikácii
- poznám kontá, ktoré mi zveríte
- na mojom serveri sú na zatiaľ neurčitú dobu uložené 
- :ref:`kontaktujte ma z aplikácie<doc_contact>`, pokiaľ požadujete úplné odstranenie akýchkoľvek dát, ktoré od vás pochádzajú