.. _doc_install:

Inštalácia
==========

Vyžaduje sa **Android 4.4 alebo vyššie**.

DM1 nie je dostupná v Google Play obchode. Stiahnuť si ju môžeš cez webový prehliadač v mobile http://bit.do/qrcode/dm1android.

Pokiaľ nemáš skúsenosti s inštaláciou Android aplikácií z neznámych zdrojov, tak skús postupovať podľa `Sťahovanie aplikácií z iných zdrojov <https://support.google.com/android/answer/7391672?hl=sk>`_.

Aktualizácie
------------

Aj keď aplikácia v súčasnosti nie je na Google Play, tak obsahuje kontrolu nových verzií. Odporúčam vźdy aktualizovať na najnovšiu verziu.

Priebeh aktualizácie nie je zámerne automatický, pozostáva z nasledujúcich krokov:

* DM1 ťa upozorní, že je k dispozícii nová verzia
* ak potvrdíš, že chceš aktualizovať, tak
* sa otvorí webový prehliadač na tvojom Androide a začne sa sťahovať novšia verzia
* stiahnutý súbor nainštaluj ako prvýkrat 
