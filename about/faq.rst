.. _doc_faq:

Často kladené otázky
====================

Prečo vznikol DM1 ?
-------------------

* nášmu 9 ročnému synovi bol diagnostikovaný diabetes mellitus 1. typu začiatkom roka 2019
* vďaka pani Pilkovej a `Občianskemu združeniu Diabetikinfo.sk <http://diabetikinfo.com/>`_ sme dostali možnosť použíať CGM firmy Medtronic
* CGM bola pre nás obrovská pomoc, no nástroje ako sledovať stav glykémie zo strany rodiča mi nepripadali dostatočné
 
  * dieťa musí mať pri sebe mobil, v ktorom je celkom dobrá aplikácia s grafom, notifikáciami aj možnosťou viesť denník
  * aplikácia je podporovaná len na veľmi obmedzenom počte zariadení
  * rodič je odkázaný len na webovú aplikáciu, čo v ňom vytvorí silnú závislosť na pravidelnom aktualizovaní webového prehliadača
  * wbová aplikácia je funkčnosťou oveľa slabšia a nepodporuje všetky moderné prehliadače
* vytváranie elektronického denníka diabetika mi nevyhovuje v žiadnej aplikácii, ktorú som vyskúšal

Ako vypnem klávesnicu, ktorá mi zakrýva dôležité informácie ?
---------------------------------------------------------------------

Stačí stlačiť Androidové tlačidlo **Späť**.

Má pre mňa DM1 význam pokiaľ nemám Medtronic Enlite CGM senzor ?
----------------------------------------------------------------

V krátkosti: zatial nie

Mám v pláne rozšíriť funkcionalitu tak, aby CGM bolo len doplnkom. Bude záležať na osobnej potrebe.

.. Je tiež pravdepodobné, že DM1 bude podporovať Abbot FreeStyle Libre a niektorý z glukomerov. 

Má DM1 význam pre pacienta, ktorý nie je dieťa ?
------------------------------------------------

V krátkosti: Neviem, to musíš zistiť sám. 

DM1 vývoj je orientovaný na rodiny s deťmi postihnutými diabetes mellitus 1. typu, ale ak sa mi časom podarí doplniť funkcie, 
ktoré sa mi dnes javia ako zaujímavé, tak to môže byt praktická pomôcka pre každého diabetika s liečbou inzulínom.

Vyžaduje DM1 Internetové pripojenie a ako veľmi ho využíva ?
------------------------------------------------------------

V krátkosti: Áno, DM1 vyžaduje pripojenie na internet.

Aplikácia využíva môj server ako bránu k CGM dátam, čo umožnuje výrazne minimalizovať veľkosť prenášaných dát. 
Plánovaná funkcionalita sa bude zameriavať na vedenie denníka diabetika a jeho zdielanie medzi viacerými užívateľmi. To je tiež veľmi malý objem dát.
Na tieto funkcie DM1 stačí pomalý, ale stále dostupný internet.

Integrácia Medtronic CareLinku v DM1 predstavuje výraznejšíu spotrebu Internetu v prípade, že práve aktívne sledujete zobrazovaný graf.

Na akých zariadeniach sa dá DM1 používať ?
------------------------------------------

V tejto chvíli je DM1 použiteľné len na mobilných zariadeniach s operačným systémom **Android**:

* Android 4.4 a vyššie (API 19)
* optimalizácia zatiaľ len na mobilné obrazovky - funčné na tabletoch, ale obrazovky 

**iPhone** v súčasnosti nemám potrebu podporovať.

**Webový prehliadač** chcem využiť na sprístupnenie prehľadov zaevidovaných údajov.

.. _doc_contact:

Ako kontaktovať autora ?
------------------------

V hlavnom menu je položka **Podpora** a tam vyber emailovú aplikáciu, ktorú používaš.