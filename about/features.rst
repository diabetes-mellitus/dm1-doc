.. _doc_features:

Vlastnosti
==========

Čo DM1 už vie
-------------

* rýchla kalkulácia sacharidových jednotiek podľa údajov z obalu potraviny
* integrácia s CGM systémom Medtronic CareLink

  * permanentne zobrazená notifikácia obsahujúca aktuálnu glykémiu a rýchlosť zmeny za posledných 15 minút
  * informácia zobrazená aj na zamknutej obrazovke mobilu
  * zvuková a svetelná (záleží od mobilu) výstraha pri hodnotách mimo rozsahu
  * zobrazenie podstatných informácií o senzore (čas kalibrácie, expirácia senzora, batéria vysielača)
  * integrovaná webová aplikácia Medtronic CareLink, ktorá sa rýchlo otvorí v DM1 bez potreby si pamätať heslo

* možnosť dočasne vypnú zobrazovanie informácií a výstrah
* upozornenie na novú verziu aplikácie
* integrácia s NightScout 

  * zobrazovanie aktuálnej hodnoty glykémie
  * otvorenie grafu NightScout priamo v aplikácii

Aká je moja aktuálna predstava rozširovania funkcionality
---------------------------------------------------------

* využívať existujúce riešenia a pridávanie funkcionality, ktorá je vhodná pre správu DM1 detí

  * ako rozumné sa javí zdielanie databázy s NightScout
  * dopracovať niektoré zostavy do NightScout

* vytváranie komunity diabetika, ktorá má možnosť vidieť, prípadne aj meniť údaje, formou skupín: diabetik, rodina, opatrovateľ, odborník a kamarát
* sledovanie počtu použitia ihiel a lanciet
* náhodné odporúčanie miesta vpichu (denného/nočného inzulínu a vpichu do prsta)
* evidencia a informácie o nároku na predpis pomôcok
* plánovanie akcií dňa

  * meranie glykémií glukomerom
  * množstvo a čas podania inzulínu
  * množstvo a čas podania jedla
  * športové aktivity
  * pripomienka pitia

* pripomínanie a rýchle potvrdzovanie akcií počas dňa
* vyhodnocovanie CGM

  * upozornenie na prekročenie hodnôt pred a po jedle a pripomenutie ako situáciu korigovať
  * vyhľadanie vhodnej glykémie na podanie jedla v približne plánovanom čase

* home screen widget s najdôležitejšimi informáciami
* vlastný zoznam potravín, jedál a ich sacharidových a energatických hodnôt
* vyhodnocovanie údajov

  * prezentácia údajov vhodná na rozhodovanie o zmene množstva inzulínu a sacharidových jednotiek
  * zdielanie grafov a tabuliek medzi rodičmi rôznych pacientov
  * výstupy vhodné pre lekára
