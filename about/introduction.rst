.. _doc_about_intro:

Úvod
============

O DM1 aplikácii
---------------

DM1 je mobilná aplikácia zameranám na rodičov detí s diagnózou **diabetes mellitus 1. typu** (autoimúnna forma cukrovky, ktorá vyžaduje inzulínovú substitúciu).

Na rozdiel od iných aplikácii sa nezameriava len na samotného diabetika, ale vytvára možnosť zapojiť do monitorovania a riadenia liečby aj jeho rodinu a okolie.

Po pridaní plánovaných funkcií, bude vhodná pre každého diabetika vyžadujúceho liečbu inzulínom. 

Podobných aplikácii je na Google Play množstvo. DM1 sa od nich odlišuje okrem zameraním aj integráciou s webovou aplikáciu **Medtronic CareLink**,

DM1 pre Android je použiteľná len s mnou prevádzkovaným serverom, ktorý slúži ako brána k CGM systémom a ako úložisko zdielaných dát.

**Ako autor sa zriekam akejkoľvek zodpovednosti za kvalitu, správnosť a funkčnosť aplikácie. Používanie DM1 je na tvoje vlastné riziko.**

Organizácia dokumentácie
------------------------

Dokumentáciu som rozdelil do 4 častí, ktoré priblížia význam aplikácie, 
objasnia ako ju začať používať, 
detailne vysvetlia každú obrazovaku 
a posledná obsahuje dodatočné informácie relevantné k diagnóze DM1.

Ako prirodzený postup čítania tejto dokumentácie mi pripadá toto poradie:

- :ref:`pár obrázkov<doc_screenshots>` môže povedať viac ako tisíc slov
- oboznám sa so stručným súhrnom :ref:`existujúcej a plánovanej funkcionality<doc_features>`
- orientačne si prebehni :ref:`často kladené otázky a odpovede<doc_faq>`
- :ref:`nainštaluj aplikáciu<doc_install>` na mobilné zariadenie
- oboznám sa s :ref:`právnymi otázkami<doc_legal>`
- a vykonaj :ref:`úvodnú konfiguráciu<doc_quickstart>` aplikácie
- detailne si prečítaj :ref:`o každej obrazovke<doc_screens>`, aby si vedel využiť maximálny potenciál DM1
- pri každej aktualizácii sa pokúsim zachytiť zoznam zmien a podľa neho sa vráť k aktualizovanej dokumentácii